package org.bitbucket.bradleysmithllc;

import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeyDurableStyleDimensionsAndFactsTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	protected Profile profile;
	private ReportingArea reportingArea;
	private Dimension dimension;
	private ColumnStore columnStore;
	private Fact fact;

	@Before
	public void prepareProfile() throws Exception
	{
		File pdir = temporaryFolder.newFolder();

		pdir.delete();

		// create profile
		profile = Profile.create(pdir);
		reportingArea = profile.defaultReportingArea();

		// create tables

		// load script
		String resource = getClass().getSimpleName() + ".txt";

		InputStream in = getClass().getClassLoader().getResourceAsStream(resource);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		StringWriter stw = new StringWriter();

		String read = null;

		while ((read = br.readLine()) != null)
		{
			stw.write(read);
			stw.write("\n");
		}

		Pattern p = Pattern.compile("\\+(\\w+)\\[([^\\]]+)\\]\n?");

		// iterate through and do what is required
		Matcher m = p.matcher(stw.toString());

		while (m.find())
		{
			String command = m.group(1).toLowerCase();
			String context = m.group(2);

			System.out.println(command + ": " + context);

			Method method = KeyDurableStyleDimensionsAndFactsTest.class.getDeclaredMethod(command, String.class);
			method.invoke(this, context);
		}

		profile.persist();
	}

	/**
	 * Create a new fact
	 * @param context
	 */
	private void fact(String context)
	{
		// schema.name
		int dot = context.indexOf('.');

		String schema = context.substring(0, dot);
		String name = context.substring(dot + 1);

		dimension = null;
		fact = reportingArea.newFact().schema("dbo").name(name).schema(schema).create();
		columnStore = fact;
	}

	/**
	 * New fact dimension key.  Context is Column,Dim,Dim Key
	 * @param context
	 */
	public void fdk(String context)
	{
		String [] bits = context.split(",");

		String col = bits[0];
		String dim = bits[1];
		String dk = bits[2];
		
		fact.newFactDimensionKey().physicalName(dk).dimension(dim).keyName(dk).join(col,col).create();
	}

	/**
	 * Create a new dimension
	 * @param context
	 */
	private void dim(String context)
	{
		// schema.name
		int dot = context.indexOf('.');

		String schema = context.substring(0, dot);
		String name = context.substring(dot + 1);

		fact = null;
		dimension = reportingArea.newDimension().schema("dbo").name(name).schema(schema).create();
		columnStore = dimension;
	}

	/**
	 * Create a new database column
	 * @param context
	 */
	private void dc(String context)
	{
		if (dimension != null)
		{
			dimension.newDatabaseColumn().name(context).jdbcType(Types.INTEGER).create();
		}
		else
		{
			fact.newDatabaseColumn().name(context).jdbcType(Types.INTEGER).create();
		}
	}

	/**
	 * Create a new dimension key.  Context is Name,Role,Columns...
	 * @param context
	 */
	private void dk(String context)
	{
		DimensionKey.DimensionKeyBuilder builder = dimension.newDimensionKey();

		String[] split = context.split(",");

		// grab first one
		String pn = split[0].trim();

		for (int i = 1; i < split.length; i++)
		{
			builder.column(split[i].trim());
		}

		builder.physicalName(pn).logicalName(pn).create();
	}

	/**
	 * Create a new reporting area
	 * @param context
	 */
	private void ra(String context)
	{
		reportingArea = profile.defaultReportingArea().newReportingArea().name(context).create();
	}

	@Test
	public void locateSks() throws IOException, SQLException
	{
		// process keys
		KeyDurableStyleDimensionsAndFacts.processProfile(profile.getProfileDirectory());

		// verify
	}
}