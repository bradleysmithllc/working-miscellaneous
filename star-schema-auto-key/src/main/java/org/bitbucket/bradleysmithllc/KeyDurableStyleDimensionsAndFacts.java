package org.bitbucket.bradleysmithllc;

import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyDurableStyleDimensionsAndFacts
{
	public static void main(String [] argv) throws IOException
	{
		processProfile(Profile.defaultProfileDir());
	}

	public static void processProfile(File profileDir) throws IOException
	{
		Profile pr = new Profile(profileDir);

		update(pr.defaultReportingArea());

		pr.persist();
	}

	private static void update(ReportingArea reportingArea)
	{
		System.out.println("\n\nProcessing Reporting Area [" + reportingArea.qualifiedName() + "]");
		System.out.println("___________________________________________________________________");

		// track keys at the RA level
		keys.clear();

		System.out.println("\nProcessing Dimensions");
		updateDims(reportingArea);

		System.out.println("\nProcessing Facts");
		updateFacts(reportingArea);

		for (Map.Entry<String, ReportingArea> child : reportingArea.children().entrySet())
		{
			update(child.getValue());
		}
	}

	private static void updateFacts(ReportingArea reportingArea)
	{
		for (Map.Entry<String, Fact> factEntry : reportingArea.facts().entrySet())
		{
			updateFact(factEntry.getValue());
		}
	}

	private static void updateFact(Fact value)
	{
		System.out.println("\nProcessing fact [" + value.qualifiedName() + "]");

		// scan through each column and look for a match in the key map
		for (Map.Entry<String, DatabaseColumn> dc : value.columns().entrySet())
		{
			String name = dc.getValue().name();

			if (keys.containsKey(name))
			{
				DimensionKey dimensionKey = keys.get(name);

				System.out.println("Found a mappable column [" + name + "] which should link to [" + dimensionKey.dimension().logicalName() + "." + dimensionKey.logicalName() + "]");

				// found a match - check for key

				Map<String, FactDimensionKey> existingKeys = value.keys(dimensionKey.dimension());

				boolean mapped = false;

				for (Map.Entry<String, FactDimensionKey> keyEntry : existingKeys.entrySet())
				{
					FactDimensionKey fdk = keyEntry.getValue();
					if (fdk.dimensionKey() == dimensionKey)
					{
						mapped = true;
					}
				}

				if (!mapped)
				{
					System.out.println("Column not mapped.  Creating a link");

					FactDimensionKey.FactDimensionKeyBuilder dkb = value.newFactDimensionKey().join(name, name).keyName(dimensionKey.logicalName()).physicalName("IDX_" + dimensionKey.dimension().name() + "_" + dimensionKey.physicalName()).dimension(dimensionKey.dimension().logicalName());

					// check for a role
					if (existingKeys.size() != 0)
					{
						dkb.roleName(name);
					}

					dkb.create();
				}
				else
				{
					System.out.println("Column already mapped.  Nothing to do.");
				}
			}
		}
	}

	private static void updateDims(ReportingArea reportingArea)
	{
		// make sure the dims match the expected key structure
		for (Map.Entry<String, Dimension> dimEntry : reportingArea.dimensions().entrySet())
		{
			updateDim(dimEntry.getValue());
		}
	}

	private static void updateDim(Dimension value)
	{
		System.out.println("\nProcessing dimension [" + value.qualifiedName() + "]");

		// check for a key with the surrogate key
		checkDimKey(value, value.name() + "_SK");
		checkDimKey(value, "DURABLE_" + value.name() + "_KEY");
	}

	private static final Map<String, DimensionKey> keys = new HashMap<String, DimensionKey>();

	private static boolean checkDimKey(Dimension value, String skName)
	{
		// see if surrogate exists
		if (value.columns().containsKey(skName))
		{
			// look through indexes and locate the surrogate key index.
			String keyLookup = skName;

			for (Map.Entry<String, DimensionKey> keyEntry : value.keys().entrySet())
			{
				// must be one column and match the SK
				DimensionKey key = keyEntry.getValue();

				List<DatabaseColumn> columns = key.columns();

				if (columns.size() == 1)
				{
					for (DatabaseColumn col : columns)
					{
						if (col.name().equals(skName))
						{
							System.out.println("Found [" + skName + "] index [" + key.logicalName() + "] on dimension [" + value.qualifiedName() + "]");
							keys.put(keyLookup, key);
							return true;
						}
					}
				}
			}

			DimensionKey dk = value.newDimensionKey().physicalName("IDX_" + skName).logicalName("IDX_" + skName).column(skName).create();
			keys.put(keyLookup, dk);

			System.out.println("Created [" + skName + "] index [" + dk.logicalName() + "] on dimension [" + value.qualifiedName() + "]");
			return false;
		}

		System.out.println("Column [" + skName + "] not present in dimension [" + value.qualifiedName() + "]");
		return false;
	}
}