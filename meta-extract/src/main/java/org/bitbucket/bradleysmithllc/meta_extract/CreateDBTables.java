package org.bitbucket.bradleysmithllc.meta_extract;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CreateDBTables
{
	public static void main(String[] args) throws IOException
	{
		process("BSEG.txt");
		process("COEP.txt");
	}

	private static void process(String s) throws IOException
	{
		InputStream in = CreateDBTables.class.getClassLoader().getResourceAsStream(s);

		readMetaData(IOUtils.toString(in));
	}

	private static final Map<String, String> NAMES = new HashMap<String, String>();
	private static final String TABLE_NAME_COL = "Table";
	private static final String KEY_INDICATOR_COL = "Key";
	private static final String DESCRIPTION_COL = "Short Description";
	private static final String ORACLE_COLUMN_NAME_COL = "Field";
	private static final String ORACLE_DATA_TYPE_COL = "Oracle Data Type";
	private static final String ORACLE_SCALE_COL = "Decimal Places";
	private static final String ORACLE_PRECISION_COL = "Oracle Length";

	private static final String DEVON_ETL_CREATED_DATE_COL = "ETL_CREATED_DATE";

	static
	{
		NAMES.put(ORACLE_COLUMN_NAME_COL, ORACLE_COLUMN_NAME_COL);
		NAMES.put(DESCRIPTION_COL, DESCRIPTION_COL);
		NAMES.put(KEY_INDICATOR_COL, KEY_INDICATOR_COL);
		NAMES.put(TABLE_NAME_COL, TABLE_NAME_COL);
		NAMES.put(ORACLE_DATA_TYPE_COL, ORACLE_DATA_TYPE_COL);
		NAMES.put(ORACLE_SCALE_COL, ORACLE_SCALE_COL);
		NAMES.put(ORACLE_PRECISION_COL, ORACLE_PRECISION_COL);
	}

	static class MetaData
	{
		Map<String, Integer> hmap = new HashMap<String, Integer>();
		Map<String, Integer> kmap = new HashMap<String, Integer>();
		String tableName;

		/* Initialize this to the longest standard column */
		int maximumColumnNameLength = DEVON_ETL_CREATED_DATE_COL.length();
	}

	private static void readMetaData(String s)
	{
		MetaData metaData = extractMetaData(s);

		LineIterator li = IOUtils.lineIterator(new StringReader(s));

		StringBuilder line = new StringBuilder();

		// create the CREATE TABLE line
		System.out.println("CREATE TABLE " + metaData.tableName + " (");
		System.out.println("\t--SAP Columns");

		// eat header row
		li.next();

		// now read the row data
		while (li.hasNext())
		{
			line.setLength(0);

			String[] thisLine = li.next().split("\t");

			String colName = thisLine[metaData.hmap.get(ORACLE_COLUMN_NAME_COL)];
			line.append("\t").append(StringUtils.rightPad(colName, metaData.maximumColumnNameLength + 1, ' '));

			String dt = thisLine[metaData.hmap.get(ORACLE_DATA_TYPE_COL)];
			String precision = thisLine[metaData.hmap.get(ORACLE_PRECISION_COL)];
			String scale = thisLine[metaData.hmap.get(ORACLE_SCALE_COL)];

			// convert number into numeric
			if (dt.equals("number"))
			{
				// check for 0 scale
				if (scale.equals("0"))
				{
					// if precision is <= 9, use INT, otherwise use BIGINT
					if (Integer.valueOf(precision) <= 9)
					{
						dt = "int";
					}
					else if (Integer.valueOf(precision) <= 18)
					{
						dt = "bigint";
					}
					else
					{
						// too big for BIGINT
						dt = "numeric";
					}
				}
				else
				{
					dt = "numeric";
				}
			}

			line.append(dt);

			// special for numeric
			if (dt.equals("numeric"))
			{
				// add precision and scale if needed
				line.append('(').append(precision);

				// check for scale
				if (!scale.equals("0"))
				{
					line.append(',').append(scale);
				}

				line.append(')');
			}
			// special for varchar
			if (dt.equals("varchar"))
			{
				String length = thisLine[metaData.hmap.get(ORACLE_PRECISION_COL)];

				line.append('(').append(length).append(')');
			}

			line.append(',');

			System.out.print(StringUtils.rightPad(line.toString(), metaData.maximumColumnNameLength + 20, ' '));
			System.out.println("--" + thisLine[metaData.hmap.get(DESCRIPTION_COL)]);
		}

		// add standard columns
		System.out.println("");
		System.out.println("\t-- Non-Oracle devon landing ETL columns");
		System.out.println("\t" + StringUtils.rightPad(DEVON_ETL_CREATED_DATE_COL, metaData.maximumColumnNameLength + 1, ' ') + "datetime,");
		System.out.println("\t" + StringUtils.rightPad("ETL_CREATED_ID", metaData.maximumColumnNameLength + 1, ' ') + "varchar(30),");
		System.out.println("\t" + StringUtils.rightPad("ETL_BATCH_ID", metaData.maximumColumnNameLength + 1, ' ') + "int");
		System.out.println(");\n");

		// create unique index if possible
		if (metaData.kmap.size() != 0)
		{
			System.out.println("CREATE UNIQUE CLUSTERED INDEX UX_" + metaData.tableName + keyTag(metaData.kmap) + " ON " + metaData.tableName);
			System.out.println("(");

			Set<String> ks = metaData.kmap.keySet();

			int i = 0;
			for (String keyCol : ks)
			{
				i++;

				System.out.print("\t");
				System.out.print(keyCol);

				if (i != ks.size())
				{
					System.out.print(",");
				}

				System.out.println();
			}
			System.out.println(");");
		}
	}

	private static String keyTag(Map<String, Integer> kmap)
	{
		StringBuilder stb = new StringBuilder("_");

		for (String keyName : kmap.keySet())
		{
			stb.append(keyName.charAt(0));
		}

		return stb.toString();
	}

	private static MetaData extractMetaData(String s)
	{
		MetaData md = new MetaData();

		// process the first line as the header names
		LineIterator li = IOUtils.lineIterator(new StringReader(s));

		String headerLine = li.next();

		//System.out.println(headerLine);

		String[] cols = headerLine.split("\t");

		for (int i = 0; i < cols.length; i++)
		{
			if (NAMES.containsKey(cols[i]))
			{
				md.hmap.put(cols[i], new Integer(i));
			}
		}

		while (li.hasNext())
		{
			String line = li.next();

			String[] thisLine = line.split("\t");

			md.tableName = thisLine[md.hmap.get(TABLE_NAME_COL)];
			String colName = thisLine[md.hmap.get(ORACLE_COLUMN_NAME_COL)];

			md.maximumColumnNameLength = Math.max(md.maximumColumnNameLength, colName.length());

			String keyInd = thisLine[md.hmap.get(KEY_INDICATOR_COL)];
			if (keyInd.equals("X"))
			{
				md.kmap.put(colName, new Integer(0));
			}
		}

		return md;
	}
}
