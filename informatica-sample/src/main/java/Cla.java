import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Cla
{
	public static void main(String [] argv) throws ParseException
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		cal.setTime(simpleDateFormat.parse("2015-01-01"));

		for (int i = 0; i < 730; i++)
		{
			cal.add(Calendar.DATE, 1);

			System.out.println(i + "\t" + simpleDateFormat.format(cal.getTime()) + " 00:00:00.000");
		}
	}

	public static void main1(String [] argv) throws ParseException
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

		cal.setTime(simpleDateFormat.parse("00:00"));

		for (int i = 0; i < 1439; i++)
		{
			cal.add(Calendar.MINUTE, 1);

			System.out.println(i + "\t" + simpleDateFormat.format(cal.getTime()));
		}
	}
}
