package org.bitbucket.bradleysmithllc;

import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileWriter;
import net.java.truevfs.access.TVFS;

import java.io.IOException;

public class App
{
	public static void main(String[] args) throws IOException
	{
		TFile file = new TFile("archive.zip");

		TFile notherFile = new TFile(file, "entry.json");

		TFileWriter tw = new TFileWriter(notherFile);

		try
		{
			tw.write("{\"timestamp\": " + System.currentTimeMillis() + "}");
		}
		finally
		{
			tw.close();
		}

		TVFS.umount();
	}
}
