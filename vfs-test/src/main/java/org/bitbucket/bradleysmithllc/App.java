package org.bitbucket.bradleysmithllc;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;

/**
 * Hello world!
 */
public class App
{
	public static void main(String[] args) throws Exception
	{
		FileSystemManager fsManager = VFS.getManager();
		FileObject jarFile = fsManager.resolveFile( "jar:lib/aJarFile.jar" );
	}
}
